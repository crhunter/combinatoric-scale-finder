# Combinatoric Scale Finder v1.0

## Description

This is an application, written in Python 3.x, is designed to explore both standard and obscure/unorthordox musical scales to the extent that "scale" is defined as one of the 2048 combinations of unique notes of the chromatic scale of any length.  It uses Tkinter for the GUI and only native python modules, so it should run as is on Linux, Windows and MacOS.
If you don't have Python, you can download the latest Python installer from <https://www.python.org/downloads/>

## TO-DO list

* Export results function
* Filter history
* Show more information for Show Scale feature

## Update History

### Version 1.0
 * 1st release


## Instructions for Use after Downloading

* type "help" in the filter box for instructions (also in help.txt)

### Starting the program
* Run the scales.py file in Python 3... simples!  :-)

## Licence

(C) Copyright 2022  Clinton Hunter

Combinatoric Scale Finder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Combinatoric Scale Finder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Combinatoric Scale Finder.  If not, see <https://www.gnu.org/licenses/>.
