import tkinter as tk
from tkinter import ttk
import itertools
import os
import re

def rotate_bits(n, d, s):
    """
    n - integer number to rotate
    d - integer number of bits to rotate 
            * negative value:  rotate right
            * positive value:  rotate left
    s - size restriction (in bits) of the rotation
    """
    if d > 0:
        return ((n << d) | (n >> (s-d))) & ((1 << s) - 1)
    #d must be zero or less
    d = abs(d)
    return ((n >> d) | (n << (s - d))) & ((1 << s) - 1)

def get_scaleInvert(scaleId):
    n = scaleId  | 4096
    r = 0
    while n > 0:
        r = r << 1 if r > 0 else 0
        r ^= (n & 1)
        n >>= 1
    return r & 4095

def get_scaleComplement(scaleId):
    n = scaleId ^ 4095
    while (n & 1) != 1:
        n >>= 1
    return n

def get_scaleModes(scaleId):
    foundModes = []
    for i in range(12):
        modeTest = rotate_bits(scaleId, -i, 12)
        if not (modeTest & 1):
            continue
        foundModes.append(modeTest)
    foundModes.pop(0)
    return foundModes

def get_scaleIntervals(scaleId):
    n = [(scaleId|4096) & (1<<i) for i in range(13)]
    intervals = []
    for startpos in range(13):
        if n[startpos] == 0:
            continue
        for endpos in range(startpos+1,13):
            if n[endpos] == 0:
                continue
            intervals.append(endpos-startpos)
            break
    return intervals

def get_allIntervalCounts(scaleId):
    scaleDegrees = [(scaleId|4096) & (1<<i) for i in range(13)]
    intervals = { k:v for (k,v) in zip([i for i in range(1,13)],[0]*12)}
    for startpos in range(13):
        if scaleDegrees[startpos] == 0:
            continue
        for endpos in range(startpos+1,13):
            if scaleDegrees[endpos] == 0:
                continue
            intervals[endpos-startpos] += 1
    return intervals 

def get_nameFromscaleId(scaleId):
    first = ['B','BR','C','CR','D','DR','F','FL','FR','G','GR','H','J','L','M','N',
             'P','PL','PR','R','S','SC','SH','ST','SW', 'T','TR','TH','V','W','Y','Z']
    second = ['A','O','E','U','I']
    third = ['B','K','D','F','G','J','L','M','N','P','Q','S','T','V','X','Z']
    return first[(scaleId >> 1) & 31] + second[(scaleId >> 6) & 3] + third[(scaleId >> 8) & 15] + 'IAN'

def scaleId_toArray(scaleId):
    out = [0]
    for i in range(1,12):
        if (scaleId & (1 << i)) > 0:
            out.append(i)
    return out


class CombinatoricScaleFinder():
    def __init__(self, root):
        self.notes = {
            'flats': ['C','Db','D','Eb','E','F','Gb','G','Ab','A','Bb','B'],
            'sharps' : ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'],
            'numbers': ['N/A']
            }
        self.displayTypes = ['numbers', 'flats','sharps']
        self.displayScale = None
        self.foundScalesVar = tk.Variable()
        self.help = open('help.txt','r').read().split('\n')
        self.labelFiltersText = "Type in filters. Press ENTER to execute. For instructions type \"help\" or select File > Help."
        self.minwidth = 600
        self.minheight = 480
        self.root = root
        self.root.geometry('%sx%s' % (self.minwidth, self.minheight))
        self.root.minsize(self.minwidth, self.minheight)
        self.root.resizable(True, True)
        self.create_widgets()
        self.setup_widgetData()
        self.bind_widgetEvents()
        self.add_widgetsToFrames()
        self.root.update()
        
    def create_widgets(self):
        #define the widgets
        self.menubar = tk.Menu(self.root)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        #self.filemenu.add_command(label="Export", command=self.exportScalesBtn_click)
        self.filemenu.add_command(label="Help", command=self.show_help)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=self.root.destroy)
        self.menubar.add_cascade(menu=self.filemenu, label="File")
        self.root.config(menu=self.menubar)
        self.labelFilters = ttk.Label(self.root)
        self.sizer = ttk.Sizegrip(self.root)
        
        self.filterEntryFrame = tk.Frame(self.root)
        self.filterEntry = ttk.Entry(self.filterEntryFrame, font='TkFixedFont')
        self.prevFilterBtn = ttk.Button(self.filterEntryFrame, text='<', width=3)
        self.nextFilterBtn = ttk.Button(self.filterEntryFrame, text='>', width=3)

        self.gridFrame = tk.Frame(self.root)
        self.labelDisplayAs = ttk.Label(self.gridFrame, text="Display As")
        self.labelKey = tk.Label(self.gridFrame, text="Select Root Note")
        self.displayAsBox = ttk.Combobox(self.gridFrame, width=10, state='readonly')
        self.keySelectBox = ttk.Combobox(self.gridFrame, width=10, state='readonly')
        
        self.filterResultFrame = tk.Frame(self.root)
        self.foundScales = tk.Listbox(self.filterResultFrame, listvariable=self.foundScalesVar, height=10, width=40, font='TkFixedFont')
        self.foundScalesScrollbar = ttk.Scrollbar(self.filterResultFrame, orient=tk.VERTICAL, command=self.foundScales.yview)

        self.scaleDegreesFrame = tk.Frame(self.root)
        self.scaleDegrees = []
        self.scaleDegreeValues = []
        self.scaleDegreeLabels = []
        for i in range(12):
            self.scaleDegreeValues.append(tk.IntVar())
            self.scaleDegrees.append(
                ttk.Checkbutton(self.scaleDegreesFrame, text=str(i), variable=self.scaleDegreeValues[i], onvalue=2**i, offvalue=0)
            )
            self.scaleDegreeValues[i].set(0)
            self.scaleDegreeLabels.append(ttk.Label(self.scaleDegreesFrame, font='TkFixedFont', text=str(i)))
            
        self.scaleDegreeValues[0].set(1)
        self.scaleDegrees[0].state(['selected','disabled'])
        self.showScaleBtn = ttk.Button(self.scaleDegreesFrame, text="Show Scale")    

    def setup_widgetData(self):
        self.displayAsBox['values'] = self.displayTypes
        self.displayAsBox.current(0)
        self.keySelectBox['values'] = self.notes['numbers']
        self.keySelectBox.current(0)
        self.labelFilters.config(text=self.labelFiltersText)

    def bind_widgetEvents(self):
        self.filterEntry.bind('<Return>', self.filterScalesBtn_click)
        self.foundScales['yscrollcommand'] = self.foundScalesScrollbar.set
        self.displayAsBox.bind('<<ComboboxSelected>>', self.change_displayType)
        self.keySelectBox.bind('<<ComboboxSelected>>', self.update_displayedScale)
        self.showScaleBtn.bind('<Button-1>', self.showScaleBtn_click)
        self.foundScales.bind('<Double-1>', self.showScaleBtn_click)

    def add_widgetsToFrames(self):
        self.labelFilters.pack(side=tk.TOP, anchor=tk.W, padx=16, pady=(16,0))       
        self.prevFilterBtn.pack(side=tk.LEFT)
        self.filterEntry.pack(side=tk.LEFT, padx=16, anchor=tk.CENTER, expand=1, fill=tk.X)
        self.nextFilterBtn.pack(side=tk.RIGHT, pady=16)
        self.labelDisplayAs.grid(row=0, column=0, padx=8, pady=(0,8), sticky=tk.W)
        self.displayAsBox.grid(row=0, column=1, padx=8, pady=(0,8))
        self.labelKey.grid(row=1, column=0, padx=8, pady=(0,8), sticky=tk.W)
        self.keySelectBox.grid(row=1, column=1, padx=8, pady=(0,8))
        for i in range(len(self.scaleDegrees)):
            self.scaleDegrees[i].pack(side=tk.LEFT, anchor=tk.CENTER, fill=tk.X, expand=1)
        self.showScaleBtn.pack()
        self.foundScales.pack(padx=(8,0), anchor=tk.W, side=tk.LEFT, expand=1, fill=tk.BOTH)
        self.foundScalesScrollbar.pack(padx=(0,8), anchor=tk.W, side=tk.LEFT, fill=tk.Y)
        #pack frames/widgets into root
        self.filterEntryFrame.pack(padx=16, side=tk.TOP, fill=tk.X, anchor=tk.W)
        self.gridFrame.pack(side=tk.TOP, anchor=tk.W, padx=16, pady=16)
        self.scaleDegreesFrame.pack(padx=32, pady=(0,16), fill=tk.X)  
        self.filterResultFrame.pack(side=tk.TOP, padx=16, pady=(0,32), anchor=tk.W, expand=1, fill=tk.BOTH)
        self.sizer.pack(anchor=tk.S, side=tk.RIGHT)
    
    def show_help(self, event=None):
        self.foundScalesVar.set(value=self.help)
    
    def filterScalesBtn_click(self, event):
        self.foundScalesVar.set(value=self.process_filters(self.filterEntry.get()))
    
    def exportScalesBtn_click(self, event):
        f = open('scales.txt', 'w')
        f.write('\n'.join(list(self.foundScales.get(0, tk.END))))
        f.close()
    
    def change_displayType(self, event):
        currentSelection = self.keySelectBox.current()
        self.keySelectBox['values'] = self.notes[self.displayAsBox.get()]
        if self.displayAsBox.get() == 'numbers':
            currentSelection = 0
            self.displayScale = None
        self.keySelectBox.current(currentSelection)
        self.update_displayedScale()
    
    def update_displayedScale(self, event=None):
        if self.displayAsBox.get() == "numbers":
            for i in range(12):
                self.scaleDegrees[i].configure(text=str(i))
            return
        newRoot = self.keySelectBox.current()
        noteArray = self.notes[self.displayAsBox.get()]
        self.displayScale = noteArray[newRoot:] + noteArray[:newRoot]
        for i in range(12):
            self.scaleDegrees[i].configure(text=self.displayScale[i])
    
    def showScaleBtn_click(self, event):
        if event.widget == self.showScaleBtn:
            scaleValue = sum([self.scaleDegreeValues[i].get() for i in range(12)])            
        elif event.widget == self.foundScales:
            try:
                scaleValue = int(self.foundScales.selection_get().split('(')[0])
            except:
                return
        self.filterEntry.delete(0, tk.END)
        self.filterEntry.insert(0,"sn%s" % scaleValue)
        self.foundScalesVar.set(value=self.display_scaleInformation(scaleValue))

    def process_filters(self, s):
        if s == '':
            return ['Error - no filters specified.']
        filterTypes = {'sl':[], 'ic':[], 'is':[], 'dx':[], 'di':[], 'sn':[]}
        filteredScales = []
        scaleSizes = []
        s = re.sub(' +', '', s)
        s = s.lower()
        filters = s.split(',')
        if 'help' in filters:
            return self.help
        for f in filters:
            filterTypes[f[0:2]].append(f[2:])
            
        #process modes only if a modes filter is specified
        if len(filterTypes['sn']) > 0:
            return self.display_scaleInformation(int(filterTypes['sn'][0]))

        #obtain the list of scale sizes first
        for f in filterTypes['sl']:
            f = f.split('-')
            if len(f) == 1:
                scaleSizes.extend(range(int(f[0]), int(f[0])+1))
            else:
                scaleSizes.extend(range(int(f[0]), int(f[1])+1))
        if len(scaleSizes) == 0:
            scaleSizes = list(range(1,13))
        else:
            scaleSizes = list(set(scaleSizes))
            scaleSizes.sort()

        #start building the list of scales
        for i in scaleSizes:
            for scale in itertools.combinations(range(1,12), i-1):
                scaleId = sum([pow(2,i) for i in scale]) | 1
                matched = True

                #filter on chromatic scale degree inclusions/exclusions
                for f in filterTypes['di']:
                    if int(f) not in scale:
                        matched = False
                        break
                if not matched:
                    continue
                        
                for f in filterTypes['dx']:
                    if int(f) in scale:
                        matched = False
                        break
                if not matched:
                    continue
                    
                #filter on step size (interval) 
                for f in filterTypes['is']:
                    if len(f) != 3:
                        #invalid filter
                        return ['Interval Structure Filter :: incorrect format',
                                'For further instructions type "help".']
                    f = [int(f[0]), f[1], int(f[2:])]
                    intervals = get_scaleIntervals(scaleId)
                    if f[1] == '=' and intervals.count(f[0]) != f[2]:
                        matched = False
                        break
                    if f[1] == '<' and intervals.count(f[0]) >= f[2]:
                        matched = False
                        break
                    if f[1] == '>' and intervals.count(f[0]) <= f[2]:
                        matched = False
                        break
                if not matched:
                    continue
                    
                #filter on step size (interval) between all notes
                for f in filterTypes['ic']:
                    if len(f) != 3:
                        #invalid filter
                        return ['Interval Count Filter :: incorrect format',
                                'For further instructions type "help".']
                    f = [int(f[0]), f[1], int(f[2:])]
                    intervals = get_allIntervalCounts(scaleId)
                    if f[1] == '=' and intervals[f[0]] != f[2]:
                        matched = False
                        break
                    if f[1] == '<' and intervals[f[0]] >= f[2]:
                        matched = False
                        break
                    if f[1] == '>' and intervals[f[0]] <= f[2]:
                        matched = False
                        break                     
                #add matched scales to display in the list box
                if matched:
                    scale = list(scale)
                    scale.insert(0,0)
                    scale.append(0)
                    filteredScales.append(self.scale_to_string(scaleId, scale))
        return filteredScales
    
    def display_scaleInformation(self, scaleId):
        outputTemplate = ("%s\n\n"
                          "Available Chords\n"
                          "--------------------\n"
                          "%s\n\n"
                          "Inverse\n"
                          "--------------------\n"
                          "%s\n\n"
                          "Modes\n"
                          "--------------------\n"
                          "%s\n\n"
                          "Complement Modes\n"
                          "--------------------\n"
                          "%s")
        thisScale = self.scale_to_string(scaleId, scaleId_toArray(scaleId))
        scaleChords = self.get_scaleChords(scaleId)
        scaleChords = '\n'.join(scaleChords)
        scaleInverse = self.scale_to_string(get_scaleInvert(scaleId), scaleId_toArray(get_scaleInvert(scaleId)))
        scaleModes = [self.scale_to_string(i, scaleId_toArray(i)) for i in get_scaleModes(scaleId)]
        scaleModes = '\n'.join(scaleModes)
        scaleComplements = [self.scale_to_string(i, scaleId_toArray(i)) for i in get_scaleModes(get_scaleComplement(scaleId))]
        scaleComplements = '\n'.join(scaleComplements)
        return str(outputTemplate % (thisScale, scaleChords, scaleInverse, scaleModes, scaleComplements)).split('\n')

    def scale_to_string(self, scaleId, scale):
        name = get_nameFromscaleId(scaleId)
        if self.displayScale is None:
            return str(scaleId).rjust(4, ' ') + "(" + name + "):" + ' ' * (8- len(name)) + str(scale)
        #change scale degrees to note letters instead
        scale = [self.displayScale[i] for i in scale]
        return str(scaleId).rjust(4, ' ') + '(%s):' % name + ' ' * (8- len(name)) + '[' + ', '.join(scale) + ']'

    def get_scaleDegreeList(self):
        if self.displayScale is None:
            return list(range(12))
        else:
            return self.displayScale

    def get_scaleChords(self, scaleId):
        degreeList = self.get_scaleDegreeList()
        x = [scaleId & (1<<i) for i in range(12)]
        scaleDegrees = []
        for i in x:
            if i==0:
                continue
            c=0
            while i > 1:
                c += 1
                i >>= 1
            scaleDegrees.append(c)
        chordType = {'Maj.':[[[4,3],[3,5],[5,4]],[0,2,1]], 'Min.':[[[3,4],[4,5],[5,3]],[0,2,1]],
                     'Dim.':[[[3,3],[3,6],[6,3]],[0,2,1]], 'Aug.':[[[4,4]],[0]],
                     'Sus2':[[[2,5],[5,5],[5,2]],[0,2,1]], 'Sus4':[[[5,2],[2,5],[5,5]],[0,2,1]] }
        intervals = []
        foundChords = {'Maj.':[], 'Min.':[], 'Dim.':[], 'Aug.':[], 'Sus2':[], 'Sus4': []}
        for i in range(3,4):
            scaleNotes = itertools.combinations(scaleDegrees,i)
            for n in scaleNotes:
                intervals = [n[k] - n[k-1] for k in range(1, len(n))]
                intervals.insert(0, n[0])
                for ct in chordType:
                    if intervals[1:] in chordType[ct][0]:                      
                        chordMatch = chordType[ct][0].index(intervals[1:])
                        chordRoot = n[chordType[ct][1][chordMatch]]
                        foundChords[ct].append(chordRoot)
                        if ct == 'Aug.':
                            foundChords[ct].append(chordRoot + 4 % 12)
                            foundChords[ct].append(chordRoot + 8 % 12)
                    foundChords[ct].sort()
                        #break
        out = []
        for ct in foundChords:
            if len(foundChords[ct]) == 0:
                continue
            for i in range(len(foundChords[ct])):
                foundChords[ct][i] = degreeList[foundChords[ct][i]]
            out.append("%s chords: %s" % (ct, foundChords[ct]))
        return out


if __name__ == '__main__':
    root = tk.Tk()
    CombinatoricScaleFinder(root)
    root.title("Combinatoric Scale Finder")
    root.mainloop()
